class Constants {
  /// Server base URL.
  static const baseUrl = 'http://localhost:6600/api';
  //static const baseUrl = 'https://ef6d-49-228-104-227.ngrok.io/api';

  /// Static server base URL.
  static const baseStaticUrl = 'http://localhost:6600';
  //static const baseStaticUrl = 'https://ef6d-49-228-104-227.ngrok.io';
}
